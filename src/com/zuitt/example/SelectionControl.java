package com.zuitt.example;

import java.util.Scanner;

public class SelectionControl {
    public static void main(String[] args){

        //java operators
        //   arithmetic  +, -, *, /, %
        //   comparison  >, < , >=, <=, ==, !=
        //   logical     &&, ||, !
        //   assignment  =

        //Selection control structure
        //  if-else
        /*  if(condition) {
            } else {
            }
        * */

        //Short Circuiting - reads conditions from left to right, if satisfied in the first, it will continue

        //ternary

        int num = 5;
        Boolean result = (num > 0) ? true : false;
        Scanner userInput = new Scanner(System.in);
        System.out.println("Enter value");

        int directionVal = userInput.nextInt();
        switch (directionVal){
            case 1:
                System.out.println("North");
                break;
            case 2:
                System.out.println("East");
                break;
            case 3:
                System.out.println("West");
                break;
            case 4:
                System.out.println("South");
                break;
            default:
                System.out.println("Invalid");
        }


    }
}

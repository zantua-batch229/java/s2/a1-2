package com.zuitt.example;

import java.util.Scanner;

public class Activity1 {
    public static void main(String[] args){

        String result;
        Scanner userInput = new Scanner(System.in);
        System.out.println("Enter year");
        int year = userInput.nextInt();
        if(year % 400 == 0){
            result = " is a leap year";
        } else if (year % 100 == 0) {
            result = " is NOT a leap year";
        } else if (year % 4 == 0) {
            result = " is a leap year";
        } else {result = " is NOT a leap year";
        }
        System.out.println(year + result);

    }
}

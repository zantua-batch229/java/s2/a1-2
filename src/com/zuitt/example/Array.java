package com.zuitt.example;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Array {
//    JAVA Collection
//      - are single unit of objects
//      - useful for manipulating relevant pieces of data that can be used in different situations
    public static void main(String[] args){

        int[] intArray = new int[5];
        intArray[0] = 201;
        intArray[1] = 1;
        intArray[2] = 25;
        intArray[3] = 50;
        intArray[4] = 98;

        System.out.println(intArray[2]);
        // it prints the memory address of the array

        //To print the intArray
        System.out.println(Arrays.toString(intArray));
        //declaring array with initialization
        String[] gender = {"male", "female", "LGBT"};

        String[][] classroom = new String[3][3];

        classroom[0][0] = "a";
        classroom[0][1] = "b";
        classroom[0][2] = "c";
        classroom[1][0] = "e";
        classroom[1][1] = "f";
        classroom[1][2] = "d";
        classroom[2][0] = "h";
        classroom[2][1] = "i";
        classroom[2][2] = "j";
        System.out.println(Arrays.toString(classroom));

        System.out.println(Arrays.deepToString(classroom));

        //ArrayList  - are resizable arrays, wherin elements can be added or removed whenever it is needed
        //syntax:  ArrayList<dataType> identifier = new ArrayList<dataType>()

        ArrayList<String> students = new ArrayList<>();
        //Add elements to arrayList
        students.add("John");
        students.add("Paul");
        System.out.println(students);

        //Declare ArrayList with values
        ArrayList<String> students2 = new ArrayList<>(Arrays.asList("John", "Paul"));
        System.out.println(students2);

        //accessing elements to an arraylist is done by the get() function
        System.out.println(students.get(1)); //gets john

        //update ArrayList element using set()

        students2.set(0, "Eric");
        System.out.println(students2);

        //adding element on a specific index
        students.add(1, "George");
        System.out.println(students);

        //removing specific element
        students.remove(1);
        System.out.println(students);

        //remove all elements
        students.clear();

        //getting the number of elements on arraylist

        System.out.println(students.size());
        System.out.println(students);

        Integer x;

        x=23;
        System.out.println(x+2);

        ArrayList<Integer> numbers = new ArrayList<>(Arrays.asList(1,2,5,123));
        System.out.println(numbers);

        //Hashmaps  -  collection of data in key-value pairs
                // in java, "keys" are also referred by the "fields"
                // wherein thw values are accessed by the "fields"
        //HashMap<dataTypeField, sataTypeValue> identifier = new HashMap<dataTypeField, sataTypeValue>();

        HashMap<String, String> jobPosition = new HashMap<>();

        jobPosition.put("Student", "Alice");
        jobPosition.put("Guard", "Dover");

        System.out.println(jobPosition);

        HashMap<String, String> students3 = new HashMap<>(){
            {
                put("Janitor", "Jose");
                put("Principal", "Josel");

            }
        };

        //accessing elements in hashmaps
        System.out.println(students3.get("Principal"));

        //updating an elemnt
        students3.replace("Janitor", "asdf");
        System.out.println(students3);
        //if field is already present, when put method is used, it will replace the value of the field

        //use of .keySet() get all the fields, .values() gets the values








    }
}
